// Berikut ialah kode spec yang sudah dikomentari. 
// Kode-kode berikut akan sangat berguna bagi DDP 2 Anda.
// Semoga beruntung! Tetap semangat!
// 
// Menginclude bits/stdc++.h dan library tcframe. Baris ini layaknya import pada python
#include <bits/stdc++.h>
#include <tcframe/spec.hpp>

// Pada C++ untuk menghindari ambiguitas, akan ada namespace yang akan menentukan fungsi apa saja yang digunakan.
// Anda disarankan untuk membaca ini.
// https://www.tutorialspoint.com/What-does-using-namespace-std-mean-in-Cplusplus
using namespace std;
using namespace tcframe;

// Kita akan mempersingkat dengan kode berikut.
// Anda disarankan untuk membaca ini.
// https://www.geeksforgeeks.org/typedef-versus-define-c/
typedef long long LL;
#define fi first
#define se second
#define pb push_back
const int MAXN = 1e5;
const int MAXQ = 1e6;
const LL MAXA = 1e13;

// Definisi class dari C++ mirip dengan bahasa lain, termasuk konsep python. Namun C++ sangat powerful.
// Anda sangat disarankan untuk membaca ini
// https://www.javatpoint.com/cpp-inheritance
class ProblemSpec : public BaseProblemSpec {

// Ini adalah access modifier protected. Anda dapat membacanya disini
// https://www.programiz.com/cpp-programming/access-modifiers.
protected:
    int N, Q;
    vector <LL> A;
    vector <int> B, C;

    void InputFormat() {
        LINE(N, Q);
        LINE(A % SIZE(N));
        LINES(B, C) % SIZE(Q);
    }

    void GradingConfig() {
        TimeLimit(1);
        MemoryLimit(64);
    }

    void StyleConfig() {
        
    }

    void Constraints() {
        CONS((1 <= N) && (N <= MAXN));
        CONS((1 <= Q) && (Q <= MAXQ));
        CONS(eachElementBetween(A, 0ll, 10000000000000ll));
        CONS(allElementIn(B, C, 0, N-1));
    }

    void Subtask1(){
        Points(100);
    }

private:

    // template <class T> berguna sebagai generics, artinya bisa menerima tipe data apa saja
    // Lagi-lagi, jangan bosan, anda dapat membaca ini.
    // https://www.geeksforgeeks.org/generics-in-c/
    template <class T>
    bool eachElementBetween(vector <T> &A, T lo, T hi){
        for(int i = 0; i < A.size(); i++) {
            if(A[i] < lo || A[i] > hi)
        		return false;
		}
		return true;
    }
    bool allElementIn(vector<int> &B, vector<int> &C, int lo, int hi) {
    	for(int i = 0; i < B.size(); i++) {
    		if(B[i] > C[i] || B[i] > hi || B[i] < lo || C[i] > hi || C[i] < lo)
    			return false;
		}
		return true;
	}
};

class TestSpec : public BaseTestSpec<ProblemSpec> {
protected:
    void SampleTestCase1() {
        Subtasks({1});
        Input({
            "6 4",
            "1 4 2 5 6 3",
            "1 2",
            "0 5",
            "2 5",
            "3 4",
        });
        Output({
            "6",
            "21",
            "16",
            "11",
        });
    }

    void BeforeTestCase(){
        A.clear();
        B.clear();
        C.clear();
    }
    
    void TestGroup1(){
        Subtasks({1});

        CASE(N = 4; Q = 4; A = {1, 1, 1, 1}; B = {0, 0, 1, 1}, C = {1, 3, 2, 3});
        // Ini adalah constructor. Mirip seperti pada python, constructor vector pada C++
        // menerima parameter ukuran dan nilai default.
//        CASE(N = MAXN; A = vector<int>(N, rnd.nextInt));
//        CASE(N = 5; makeRandomArray(1, MAXA));
		for(int i = 0; i < 20; i++)
			CASE(N = rnd.nextInt(MAXN/2, MAXN); Q = rnd.nextInt(MAXQ/2, MAXQ); makeRandomArray(1000000000ll, 10000000000000ll); makeBandC(0, N-1));
    }

private:
    void makeRandomArray(LL lo = 1, LL hi = MAXA){
        A.resize(N);
        for(int i = 0;i < N;i++) A[i] = rnd.nextInt(lo, hi);
    }
    void makeBandC(int lo, int hi) {
    	B.resize(Q);
    	C.resize(Q);
    	for(int i = 0; i < Q; i++) {
    		B[i] = rnd.nextInt(lo, hi);
    		C[i] = rnd.nextInt(B[i], hi);
		}
	}
};

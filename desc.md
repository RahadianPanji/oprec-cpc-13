## Jumlah Penduduk

### Deskripsi
Pak Dengklek memiliki $N$ kandang yang dinomori $0, 1, ... , N-1$. Masing-masing kandang berisi bebek yang jumlahnya beragam. Kandang ke-$i$ berisi $A$<sub>$i$</sub> bebek, dengan $0 \leq i \lt N$. 

Pak Dengklek ingin mengetahui nilai kesejahteraan bebek-bebeknya, sehingga Pak Dengklek perlu mengetahui berapa banyak bebek yang ada dalam rentang $l$ dan $r$, $0 \leq l \leq r \lt N$. Banyak bebek yang ada dalam rentang $l$ dan $r$ didefinisikan dengan $A$<sub>$l$</sub> $+$ $A$<sub>$l+1$</sub> $+$ $...$ $+$ $A$<sub>$r-1$</sub> $+$ $A$<sub>$r$</sub>. 

Pak Dengklek meminta bantuanmu, kepala divisi perbebekan, untuk menjawab pertanyaan-pertanyaan Pak Dengklek. Ia akan mengajukan $Q$ pertanyaan, dengan setiap pertanyaan berisi $l$<sub>$i$</sub> dan $r$<sub>$i$</sub>, $0 \leq i \lt Q$. Karena tidak ingin kehilangan jabatanmu, kamu pun mulai menghitung.

### Masukan

Baris pertama berisi 2 buah bilangan $N$ dan $Q$ yang menyatakan berapa kandang yang Pak Denglek miliki, dan berapa pertanyaan yang akan Pak Dengklek tanyakan.

Baris kedua berisi $N$ bilangan $A$<sub>$0$</sub>$, A$<sub>$1$</sub>$, ..., A$<sub>$N-1$</sub>, banyaknya bebek tiap kandang.

$Q$ baris berikutnya, masing-masing berisi 2 buah bilangan $l$<sub>$i$</sub> dan $r$<sub>$i$</sub>.

### Keluaran

$Q$ baris dengan setiap baris berisi 1 buah bilangan yaitu jumlah bebek-bebek yang ada dalam rentang $l$<sub>$i$</sub> dan $r$<sub>$i$</sub>, $0 \leq i \lt Q$

### Contoh Masukan

```
6 4
1 4 2 5 6 3
1 2
0 5
2 5
3 4
```

### Contoh Keluaran

```
6
21
16
11
```

### Penjelasan

Pertanyaan pertama: 4 + 2 = 6
<br>Pertanyaan kedua: 1 + 4 + 2 + 5 + 6 + 3 = 21
<br>Pertanyaan ketiga: 2 + 5 + 6 + 3 = 16
<br>Pertanyaan keempat: 5 + 6 = 11

### Batasan

- $1 \leq N \leq 10$<sup>$5$</sup>
- $1 \leq Q \leq 10$<sup>$6$</sup>
- $0 \leq A$<sub>$i$</sub> $\leq 10$<sup>$13$</sup>$, 0 \leq i \lt N$
- $0 \leq l$<sub>$i$</sub> $\leq r$<sub>$i$</sub> $\lt N,$ $0 \leq i \lt Q$
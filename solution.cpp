#include <iostream>
using namespace std;
typedef long long ll;

int main() {
	int n, q; scanf("%d %d", &n, &q);
	ll a[n+9], presum[n+9];
	for(int i = 0; i < n; i++)
		scanf("%lld", &a[i]);
	presum[0] = a[0];
	for(int i = 1; i < n; i++) {
		presum[i] = presum[i-1]+a[i];
	}
	while(q--) {
		int l, r; scanf("%d %d", &l, &r);
		if(l==0) {
			printf("%lld\n", presum[r]);
		}else {
			printf("%lld\n", presum[r]-presum[l-1]);
		}
	}
}
